package hard.code.team.controller;

import hard.code.team.models.Pill;
import hard.code.team.services.PillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/v1/pills")
public class PiilsController {
    private PillService pillService;

    @Autowired
    public PiilsController(PillService pillService) {
        this.pillService = pillService;
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public Pill createPill(@RequestBody Pill pill) {
        return pillService.createPill(pill);
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public List<Pill> getPills() {
        return pillService.getPills();
    }
}
