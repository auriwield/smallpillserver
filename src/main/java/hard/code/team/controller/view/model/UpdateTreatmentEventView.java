package hard.code.team.controller.view.model;

import java.sql.Timestamp;

public class UpdateTreatmentEventView {
    private Timestamp timestamp;

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }
}
