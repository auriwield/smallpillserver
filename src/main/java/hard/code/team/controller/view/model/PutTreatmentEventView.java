package hard.code.team.controller.view.model;

import java.sql.Timestamp;

public class PutTreatmentEventView {
    private Long scheduleId;
    private Timestamp timestamp;

    public Long getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(Long scheduleId) {
        this.scheduleId = scheduleId;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }
}
