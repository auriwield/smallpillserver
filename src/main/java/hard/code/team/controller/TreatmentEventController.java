package hard.code.team.controller;

import hard.code.team.controller.view.model.PutTreatmentEventView;
import hard.code.team.controller.view.model.UpdateTreatmentEventView;
import hard.code.team.models.TreatmentEvent;
import hard.code.team.services.TreatmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/treatment-event")
public class TreatmentEventController {
    private TreatmentService treatmentService;

    @Autowired
    public TreatmentEventController(TreatmentService treatmentService) {
        this.treatmentService = treatmentService;
    }

    @RequestMapping(value="/", method = RequestMethod.POST)
    public TreatmentEvent putTreatmentEvent(@RequestBody PutTreatmentEventView view) {
        return treatmentService.createTreatmentEvent(view.getScheduleId(), view.getTimestamp());
    }

    @RequestMapping(value="/", method = RequestMethod.GET)
    public List<TreatmentEvent> getTreatmentEvent() {
        return treatmentService.getTreatmentEvent();
    }

    @RequestMapping(value = "/{id}/set-reminded", method = RequestMethod.POST)
    public TreatmentEvent setReminded(@PathVariable Long id, @RequestBody UpdateTreatmentEventView view) {
        return treatmentService.setReminded(id, view.getTimestamp());
    }

    @RequestMapping(value = "/{id}/set-completed", method = RequestMethod.POST)
    public TreatmentEvent setCompleted(@PathVariable Long id, @RequestBody PutTreatmentEventView view) {
        return treatmentService.setCompleted(id, view.getTimestamp());
    }
}
