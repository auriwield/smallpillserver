package hard.code.team.controller;

import hard.code.team.models.Schedule;
import hard.code.team.services.ScheduleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/v1/schedule")
public class ScheduleController {
    private ScheduleService scheduleService;

    @Autowired
    public ScheduleController(ScheduleService scheduleService) {
        this.scheduleService = scheduleService;
    }

    @RequestMapping(value="/", method = RequestMethod.POST)
    public Schedule putTreatmentEvent(@RequestBody Schedule schedule) {
       return scheduleService.createSchedule(schedule);
    }

    @RequestMapping(value = "/actual")
    public List<Schedule> getSchedule() {
        return this.scheduleService.getActualSchedules();
    }


}
