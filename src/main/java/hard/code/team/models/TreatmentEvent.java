package hard.code.team.models;

import java.sql.Timestamp;

public class TreatmentEvent {
    private Long id;
    private Schedule schedule;
    private Timestamp createdAt;
    private Timestamp remindAt;
    private Timestamp completedAt;
    private Integer remindsCount;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Schedule getSchedule() {
        return schedule;
    }

    public void setSchedule(Schedule schedule) {
        this.schedule = schedule;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public Timestamp getRemindAt() {
        return remindAt;
    }

    public void setRemindAt(Timestamp remindAt) {
        this.remindAt = remindAt;
    }

    public Timestamp getCompletedAt() {
        return completedAt;
    }

    public void setCompletedAt(Timestamp completedAt) {
        this.completedAt = completedAt;
    }

    public Integer getRemindsCount() {
        return remindsCount;
    }

    public void setRemindsCount(Integer remindsCount) {
        this.remindsCount = remindsCount;
    }
}
