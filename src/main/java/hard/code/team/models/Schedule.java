package hard.code.team.models;

import net.redhogs.cronparser.CronExpressionDescriptor;

import java.sql.Timestamp;
import java.text.ParseException;

public class Schedule {

    private Long id;
    private Pill pill;
    private Timestamp fromDate;
    private Timestamp toDate;
    private String cron;
    private String dose;
    private String description;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Pill getPill() {
        return pill;
    }

    public void setPill(Pill pill) {
        this.pill = pill;
    }

    public Timestamp getFromDate() {
        return fromDate;
    }

    public void setFromDate(Timestamp fromDate) {
        this.fromDate = fromDate;
    }

    public Timestamp getToDate() {
        return toDate;
    }

    public void setToDate(Timestamp toDate) {
        this.toDate = toDate;
    }

    public String getCron() {
        return cron;
    }

    public void setCron(String cron) {
        this.cron = cron;
    }

    public String getDose() {
        return dose;
    }

    public void setDose(String dose) {
        this.dose = dose;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getHumanizedCron() {
        try {
            return CronExpressionDescriptor.getDescription(this.cron);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }
}
