package hard.code.team.dao;

import hard.code.team.models.Schedule;

import java.util.List;

public interface ScheduleDao {
    Schedule getWithId(Long id);
    List<Schedule> getActualSchedules() throws Exception ;
    Schedule createSchedule(Schedule schedule);
}
