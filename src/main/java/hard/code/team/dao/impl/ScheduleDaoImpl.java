package hard.code.team.dao.impl;

import hard.code.team.dao.ScheduleDao;
import hard.code.team.models.Pill;
import hard.code.team.models.Schedule;
import org.joda.time.DateTime;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;

import javax.sql.DataSource;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ScheduleDaoImpl implements ScheduleDao {
    private JdbcTemplate jdbcTemplate;


    public ScheduleDaoImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public Schedule getWithId(Long id) {
        List<Schedule> result = this.jdbcTemplate.query(
                "SELECT * " +
                        "FROM schedule " +
                        "JOIN pills " +
                        "ON schedule.pill_id = pills.id " +
                        "WHERE schedule.id=?",

                new Object[]{id}, (resultSet, i) -> {
                    Schedule schedule = new Schedule();
                    Pill pill = new Pill();
                    pill.setName(resultSet.getString("pills.name"));
                    schedule.setId(resultSet.getLong("schedule.id"));
                    schedule.setPill(pill);
                    schedule.setFromDate(resultSet.getTimestamp("schedule.from_date"));
                    schedule.setToDate(resultSet.getTimestamp("schedule.to_date"));
                    schedule.setCron(resultSet.getString("schedule.cron"));
                    schedule.setDose(resultSet.getString("schedule.dose"));
                    schedule.setDescription(resultSet.getString("schedule.description"));
                    return schedule;
                });

        if (result.isEmpty()) {
            return null;
        } else {
            return result.get(0);
        }
    }

    public List<Schedule> getActualSchedules() throws Exception {
        return this.jdbcTemplate.query("SELECT * FROM schedule " +
                        "JOIN pills " +
                        "ON schedule.pill_id=pills.id " +
                        "WHERE schedule.to_date >= ? ",
                new Object[]{new Timestamp(DateTime.now().getMillis())},
                (resultSet, i) -> {
                    Pill pill = new Pill();
                    pill.setId(resultSet.getLong("pills.id"));
                    pill.setName(resultSet.getString("pills.name"));
                    Schedule schedule = new Schedule();
                    schedule.setId(resultSet.getLong("schedule.id"));
                    schedule.setPill(pill);
                    schedule.setFromDate(resultSet.getTimestamp("schedule.from_date"));
                    schedule.setToDate(resultSet.getTimestamp("schedule.to_date"));
                    schedule.setCron(resultSet.getString("schedule.cron"));
                    schedule.setDose(resultSet.getString("schedule.dose"));
                    schedule.setDescription(resultSet.getString("schedule.description"));
                    return schedule;
                });
    }

    @Override
    public Schedule createSchedule(Schedule schedule) {
        SimpleJdbcInsert simpleJdbcInsert = new SimpleJdbcInsert(this.jdbcTemplate);
        simpleJdbcInsert.withTableName("schedule");
        simpleJdbcInsert.usingGeneratedKeyColumns("id");
        Map<String, Object> params = new HashMap<>();
        params.put("pill_id", schedule.getPill().getId());
        params.put("from_date", schedule.getFromDate());
        params.put("to_date", schedule.getToDate());
        params.put("cron", schedule.getCron());
        params.put("dose", schedule.getDose());
        params.put("description", schedule.getDescription());
        Number number = simpleJdbcInsert.executeAndReturnKey(params);
        return getWithId(number.longValue());
    }
}
