package hard.code.team.dao.impl;

import hard.code.team.dao.PillDao;
import hard.code.team.models.Pill;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class PillDaoImpl implements PillDao {
    private JdbcTemplate jdbcTemplate;

    public PillDaoImpl(DataSource dataSource){
        this.jdbcTemplate = new JdbcTemplate(dataSource);

    }

    @Override
    public Pill getWithId(Long id) {
        List<Pill> result = this.jdbcTemplate.query("SELECT id, name FROM pills WHERE id=?", new Object[]{id}, (resultSet, i) -> {
            Pill pill = new Pill();
            pill.setId(resultSet.getLong("id"));
            pill.setName(resultSet.getString("name"));
            return pill;
        });
        return result.isEmpty() ? null : result.get(0);
    }

    @Override
    public Pill createPill(Pill pill) {
        SimpleJdbcInsert simpleJdbcInsert = new SimpleJdbcInsert(this.jdbcTemplate);
        simpleJdbcInsert.withTableName("pills");
        simpleJdbcInsert.usingGeneratedKeyColumns("id");
        Map<String, Object> params = new HashMap<>();
        params.put("name", pill.getName());
        Number number = simpleJdbcInsert.executeAndReturnKey(params);
        return getWithId(number.longValue());
    }

    @Override
    public List<Pill> getPills() {
        List<Pill> result = this.jdbcTemplate.query("SELECT id, name FROM pills ", new Object[]{},
                (resultSet, i) -> {
                    Pill pill = new Pill();
                    pill.setId(resultSet.getLong("id"));
                    pill.setName(resultSet.getString("name"));
                    return pill;
                });
        return result.isEmpty() ? null : result;
    }
}
