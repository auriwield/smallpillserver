package hard.code.team.dao.impl;

import hard.code.team.dao.TreatmentEventDao;
import hard.code.team.models.Pill;
import hard.code.team.models.Schedule;
import hard.code.team.models.TreatmentEvent;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;

import javax.sql.DataSource;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TreatmentEventDaoImpl implements TreatmentEventDao {
    private JdbcTemplate jdbcTemplate;

    public TreatmentEventDaoImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);

    }

    @Override
    public TreatmentEvent getWithId(Long id) {
        List<TreatmentEvent> result = this.jdbcTemplate.query("SELECT" +
                        " treatment_event.id, " +
                        " treatment_event.created_at," +
                        " treatment_event.remind_at, " +
                        "treatment_event.completed_at," +
                        " treatment_event.reminds_count " +
                        "FROM treatment_event " +
                        "WHERE treatment_event.id=?",
                new Object[]{id}, (resultSet, i) -> {
                    TreatmentEvent treatmentEvent = new TreatmentEvent();
                    treatmentEvent.setId(resultSet.getLong("treatment_event.id"));
                    treatmentEvent.setCreatedAt(resultSet.getTimestamp("treatment_event.created_at"));
                    treatmentEvent.setRemindAt(resultSet.getTimestamp("treatment_event.remind_at"));
                    treatmentEvent.setCompletedAt(resultSet.getTimestamp("treatment_event.completed_at"));
                    treatmentEvent.setRemindsCount(resultSet.getInt("treatment_event.reminds_count"));
                    return treatmentEvent;
                });
        return result.isEmpty() ? null : result.get(0);
    }

    @Override
    public TreatmentEvent createTreatmentEvent(Schedule schedule) {
        SimpleJdbcInsert simpleJdbcInsert = new SimpleJdbcInsert(this.jdbcTemplate);
        simpleJdbcInsert.withTableName("treatment_event");
        simpleJdbcInsert.usingGeneratedKeyColumns("id", "created_at", "remind_at", "reminds_count");
        Map<String, Object> params = new HashMap<>();
        params.put("schedule_id", schedule.getId());
        Number number = simpleJdbcInsert.executeAndReturnKey(params);
        return getWithId(number.longValue());
    }

    @Override
    public TreatmentEvent setReminded(Long id, Timestamp timestamp) {
        this.jdbcTemplate.update("UPDATE treatment_event SET remind_at=?", timestamp);
        return this.getWithId(id);
    }

    @Override
    public TreatmentEvent setCompleted(Long id, Timestamp timestamp) {
        this.jdbcTemplate.update("UPDATE treatment_event SET completed_at=?", timestamp);
        return this.getWithId(id);
    }

    @Override
    public List<TreatmentEvent> getTreatmentEvent() {

        return this.jdbcTemplate.query("SELECT * FROM treatment_event " +
                        "JOIN schedule ON treatment_event.schedule_id = schedule.id " +
                        "JOIN pills ON schedule.pill_id = pills.id", new Object[]{},
                (resultSet, i) -> {
                    Pill pill = new Pill();
                    pill.setId(resultSet.getLong("pills.id"));
                    pill.setName(resultSet.getString("pills.name"));
                    Schedule schedule = new Schedule();
                    schedule.setId(resultSet.getLong("schedule.id"));
                    schedule.setPill(pill);
                    schedule.setFromDate(resultSet.getTimestamp("schedule.from_date"));
                    schedule.setToDate(resultSet.getTimestamp("schedule.to_date"));
                    schedule.setCron(resultSet.getString("schedule.cron"));
                    schedule.setDose(resultSet.getString("schedule.dose"));
                    schedule.setDescription(resultSet.getString("schedule.description"));
                    TreatmentEvent treatmentEvent = new TreatmentEvent();
                    treatmentEvent.setId(resultSet.getLong("id"));
                    treatmentEvent.setSchedule(schedule);
                    treatmentEvent.setCreatedAt(resultSet.getTimestamp("treatment_event.created_at"));
                    treatmentEvent.setRemindAt(resultSet.getTimestamp("treatment_event.remind_at"));
                    treatmentEvent.setCompletedAt((resultSet.getTimestamp("treatment_event.completed_at")));
                    treatmentEvent.setRemindsCount(resultSet.getInt("treatment_event.reminds_count"));
                    return treatmentEvent;
                });
    }
}
