package hard.code.team.dao;

import hard.code.team.models.Schedule;
import hard.code.team.models.TreatmentEvent;

import java.sql.Timestamp;
import java.util.List;

public interface TreatmentEventDao {
    TreatmentEvent getWithId(Long id);
    TreatmentEvent createTreatmentEvent(Schedule schedule);
    TreatmentEvent setReminded(Long id, Timestamp timestamp);
    TreatmentEvent setCompleted(Long id, Timestamp timestamp);
    List<TreatmentEvent> getTreatmentEvent();
}
