package hard.code.team.dao;

import hard.code.team.models.Pill;

import java.util.List;

public interface PillDao {
    Pill getWithId(Long id);
    Pill createPill(Pill pill);
    List<Pill> getPills();
}
