package hard.code.team.services;

import hard.code.team.models.Schedule;

import java.util.List;

public interface ScheduleService {
    Schedule getWithId(Long id);
    List<Schedule> getActualSchedules();
    Schedule createSchedule(Schedule schedule);
}
