package hard.code.team.services;

import hard.code.team.models.TreatmentEvent;

import java.sql.Timestamp;
import java.util.List;

public interface TreatmentService {
    TreatmentEvent getWithId(Long id);
    TreatmentEvent createTreatmentEvent(Long scheduleId, Timestamp timestamp);
    TreatmentEvent setReminded(Long id, Timestamp timestamp);
    TreatmentEvent setCompleted(Long id, Timestamp timestamp);
    List<TreatmentEvent> getTreatmentEvent();
}
