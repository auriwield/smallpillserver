package hard.code.team.services;

import hard.code.team.models.Pill;

import java.util.List;

public interface PillService {
    Pill getWithId(Long id);
    Pill createPill(Pill pill);
    List<Pill> getPills();
}



















