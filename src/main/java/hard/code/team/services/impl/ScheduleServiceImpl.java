package hard.code.team.services.impl;

import hard.code.team.dao.ScheduleDao;
import hard.code.team.models.Schedule;
import hard.code.team.services.ScheduleService;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public class ScheduleServiceImpl implements ScheduleService {
    private ScheduleDao scheduleDao;

    public ScheduleServiceImpl(ScheduleDao scheduleDao) {
        this.scheduleDao = scheduleDao;
    }

    @Override
    @Transactional(readOnly = true)
    public Schedule getWithId(Long id) {
        return scheduleDao.getWithId(id);
    }

    @Override
    @Transactional
    public Schedule createSchedule(Schedule schedule) {
        return scheduleDao.createSchedule(schedule);
    }

    @Override
    public List<Schedule> getActualSchedules() {
        try {
            return scheduleDao.getActualSchedules();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
