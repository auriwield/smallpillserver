package hard.code.team.services.impl;

import hard.code.team.dao.PillDao;
import hard.code.team.models.Pill;
import hard.code.team.services.PillService;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public class PillServiceImpl implements PillService {
    private PillDao pillDao;

    public PillServiceImpl(PillDao pillDao) {
        this.pillDao = pillDao;
    }

    @Override
    @Transactional(readOnly = true)
    public Pill getWithId(Long id) {
        return this.pillDao.getWithId(id);
    }

    @Override
    @Transactional
    public Pill createPill(Pill pill) {
        return pillDao.createPill(pill);
    }

    @Override
    public List<Pill> getPills() {
        return pillDao.getPills();
    }
}