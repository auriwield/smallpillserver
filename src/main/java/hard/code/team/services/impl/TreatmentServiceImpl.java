package hard.code.team.services.impl;

import hard.code.team.dao.TreatmentEventDao;
import hard.code.team.models.Schedule;
import hard.code.team.models.TreatmentEvent;
import hard.code.team.services.ScheduleService;
import hard.code.team.services.TreatmentService;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.List;

public class TreatmentServiceImpl implements TreatmentService {
    private TreatmentEventDao treatmentEventDao;
    private ScheduleService scheduleService;

    public TreatmentServiceImpl(TreatmentEventDao treatmentEventDao, ScheduleService scheduleService) {
        this.treatmentEventDao = treatmentEventDao;
        this.scheduleService = scheduleService;
    }

    @Override
    @Transactional(readOnly = true)
    public TreatmentEvent getWithId(Long id) {
        return this.treatmentEventDao.getWithId(id);
    }

    @Override
    @Transactional
    public TreatmentEvent createTreatmentEvent(Long scheduleId, Timestamp timestamp) {
        Schedule schedule = scheduleService.getWithId(scheduleId);
        return treatmentEventDao.createTreatmentEvent(schedule);
    }

    @Override
    @Transactional
    public TreatmentEvent setReminded(Long id, Timestamp timestamp) {
        return this.treatmentEventDao.setReminded(id, timestamp);
    }

    @Override
    @Transactional
    public TreatmentEvent setCompleted(Long id, Timestamp timestamp) {
        return treatmentEventDao.setCompleted(id, timestamp);
    }

    @Override
    public List<TreatmentEvent> getTreatmentEvent() {
        return treatmentEventDao.getTreatmentEvent();
    }
}
